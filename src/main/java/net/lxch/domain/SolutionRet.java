package net.lxch.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
public class SolutionRet {
    private long distance;
    private List<Integer> router = new ArrayList<>();
}
