package net.lxch.domain;

import lombok.*;

/**
 * 内部计算节点
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
public class InterNode {

    private int id;

    private Point point;

    private String recordId;
}
