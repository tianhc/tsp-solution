package net.lxch.domain;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 配送单
 */
@Getter
@Setter
@ToString
public class ProductRecord {

    /**
     * 配送单编号
     */
    private String id;

    /**
     * 地址
     */
    private String address;

    /**
     * 配送坐标
     * 小数点后保留6位
     */
    private Point point;
}
