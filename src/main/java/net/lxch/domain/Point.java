package net.lxch.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 坐标点
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
public class Point {
    /**
     * 纬度
     * 小数点后六位
     */
    private Double lat;
    /**
     * 经度
     * 小数点后六位
     */
    private Double lng;
}
