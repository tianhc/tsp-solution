package net.lxch.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * 最短配送路径
 */
@Getter
@Setter
@ToString
public class MinDistance {
    /**
     * 最短路径规划
     */
    private List<Integer> router;

    /**
     * 最短距离
     */
    private Double distance;
}
