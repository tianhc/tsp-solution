package net.lxch.domain;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class AmapDistanceRet {
    private String status;
    private Route route;

    @Data
    public static class Route {
        private List<Path> paths;
    }

    @Data
    public static class Path {
        private String distance;
        private String duration;
    }
}
