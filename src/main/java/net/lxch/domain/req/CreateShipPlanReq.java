package net.lxch.domain.req;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import net.lxch.domain.Point;
import net.lxch.domain.ProductRecord;

import java.util.List;

@Getter
@Setter
@ToString
public class CreateShipPlanReq {
    /**
     * 起始点
     */
    private Point originPoint;
    /**
     * 待配送货品
     */
    private List<ProductRecord> productRecords;
}
