package net.lxch.domain.resp;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import net.lxch.domain.InterNode;
import net.lxch.domain.ProductRecord;

import java.util.List;

/**
 * 配送计划
 */
@Getter
@Setter
@ToString
public class ShipPlan {
    /**
     * 配送计划
     */
    private List<ProductRecord> plan = Lists.newArrayList();
    /**
     * 总距离 米
     */
    private Long distance;
    /**
     * 路由节点 包含起始点
     */
    private List<InterNode> route = Lists.newArrayList();

}
