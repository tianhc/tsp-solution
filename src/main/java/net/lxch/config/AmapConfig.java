package net.lxch.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * 高德地图配置
 */
@Getter
@Setter
@ToString
@Configuration
public class AmapConfig {
    @Value("${amap.key:6e97b005c5dd79912d9fd6b7d4476a04}")
    private String amapKey;

    @Value("${amap.secret:71f2db903f4b38837e67229e8bb2dd9d}")
    private String amapSecret;
}
