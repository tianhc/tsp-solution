package net.lxch.service;

import net.lxch.domain.AmapDistanceRet;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "amapService",url = "https://restapi.amap.com/v3")
public interface AmapService {
    @GetMapping("/direction/driving")
    AmapDistanceRet getDrivingDirection(@RequestParam("origin") String origin,
                                        @RequestParam("destination") String destination,
                                        @RequestParam("output") String output,
                                        @RequestParam("key") String key,
                                        @RequestParam("sig") String sig);
}
