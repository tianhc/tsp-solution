package net.lxch.service;

import net.lxch.domain.Point;
import net.lxch.domain.ProductRecord;
import net.lxch.domain.resp.ShipPlan;

import java.util.List;

public interface ShipPlanService {
    /**
     * 根据起点和配送货物列表获取最短配送路径规划
     */
    ShipPlan getShortShipPlan(Point originPoint, List<ProductRecord> productRecords);
}
