package net.lxch.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.lxch.domain.req.CreateShipPlanReq;
import net.lxch.domain.req.R;
import net.lxch.domain.resp.ShipPlan;
import net.lxch.service.ShipPlanService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequestMapping("/ship-plan")
@RestController
@RequiredArgsConstructor
public class ShipPlanController {
    private final ShipPlanService shipPlanService;

    @PostMapping("/create")
    public R<ShipPlan> createShipPlan(@RequestBody CreateShipPlanReq req){
        return R.ok(shipPlanService.getShortShipPlan(req.getOriginPoint(), req.getProductRecords()));
    }
}
