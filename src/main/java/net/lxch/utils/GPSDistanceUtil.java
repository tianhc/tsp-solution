package net.lxch.utils;

import net.lxch.domain.Point;

public class GPSDistanceUtil {
    // 6378.137为地球半径(单位:千米)
    private static final double EARTH_RADIUS = 6378.137;

    /**
     * 距离 米
     * @param point1
     * @param point2
     * @return
     */
    public static long getDistance(Point point1, Point point2) {
        double radLat1 = rad(point1.getLat());
        double radLat2 = rad(point2.getLat());
        // a 是两坐标点的纬度之差
        double a = radLat1 - radLat2;
        // b 是两坐标点的经度之差
        double b = rad(point1.getLng()) - rad(point2.getLng());

        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) +
                Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)));
        s = s * EARTH_RADIUS;
        // 转为米，用 Math.round() 取整
        s = Math.round(s * 1000);
        return Math.round(s);
    }

    // 角度转弧度
    // 把用角度表示的角转换为近似相等的用弧度表示的角 Math.toRadians。
    // 经纬度是以度数（0-360）表示。如果要把度数转换为弧度（0-2π），要除以360再乘以2ππ（相当于，乘以π/ 180）。
    private static double rad(double d) {
        return d * Math.PI / 180.0;
    }
}
