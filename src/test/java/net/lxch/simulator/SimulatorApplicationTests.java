package net.lxch.simulator;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import net.lxch.domain.Point;
import net.lxch.domain.ProductRecord;
import net.lxch.service.ShipPlanServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@Slf4j
@SpringBootTest
class SimulatorApplicationTests {
    @Resource
    private ShipPlanServiceImpl shipPlanService;

    /**
     * 调用高德接口获取两点间距离
     */
    @Test
    public void getDirectionTest() {
        Point point1 = new Point(36.986166, 116.87876);
        Point point2 = new Point(36.73798, 117.400337);
        log.info("获取两点之间距离 {}", shipPlanService.getDirection(point1, point2));
    }

    @Test
    public void getShipPlan() {
        List<ProductRecord> records = Lists.newArrayList();
//        for (int i = 0; i < 100; i = i + 3) {
        int i=0;
//             货品1
            ProductRecord record1 = new ProductRecord();
            record1.setId("A"+String.valueOf(i));
            record1.setPoint(new Point(36.986166, 116.87876));
            record1.setAddress("齐河");
            records.add(record1);
            // 货品2
            ProductRecord record2 = new ProductRecord();
            record2.setId("B"+String.valueOf(i + 1));
            record2.setPoint(new Point(36.73798, 117.400337));
            record2.setAddress("龙泉大道");
            records.add(record2);
            // 货品3
            ProductRecord record3 = new ProductRecord();
            record3.setId("C"+String.valueOf(i + 2));
            record3.setPoint(new Point(36.603669, 117.307002));
            record3.setAddress("历城");
            records.add(record3);
//        }

        log.info("最优配送计划 {}", shipPlanService.getShortShipPlan(new Point(36.690668, 117.032488), records));
    }
}
