package net.lxch.simulator;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Test {
    static int[] marks = new int[100];//标记某个数是否被用过,可以自己调整大小
    static int[] nums = new int[100];
    static int n;

    public static void main(String[] args) {
//        List<ProductRecord> records = Lists.newArrayList();
//
//        for (int i = 0; i < 3; i = i + 3) {
//            // 货品1
//            ProductRecord record1 = new ProductRecord();
//            record1.setId(String.valueOf(i));
//            record1.setPoint(new Point(36.986166, 116.87876));
//            record1.setAddress("齐河");
//            records.add(record1);
//            // 货品2
//            ProductRecord record2 = new ProductRecord();
//            record2.setId(String.valueOf(i + 1));
//            record2.setPoint(new Point(36.73798, 117.400337));
//            record2.setAddress("龙泉大道");
//            records.add(record2);
//            // 货品3
//            ProductRecord record3 = new ProductRecord();
//            record3.setId(String.valueOf(i + 2));
//            record3.setPoint(new Point(36.603669, 117.307002));
//            record3.setAddress("历城");
//            records.add(record3);
//        }
//        Simulator simulator = new Simulator(new Point(36.690668, 117.032488), records);
//        simulator.simulate();
        n = 10;
        d(1);
    }

    public static void d(int step) {

        if (step == n + 1) {
            System.out.println();
            for (int i = 1; i <= n; i++) {
                System.out.print(nums[i] + " ");
            }
            return;

        }


        for (int i = 1; i <= n; i++) {
            if (marks[i] == 0) {
                nums[step] = i;
                marks[i] = 1;

                d(step + 1);
                marks[i] = 0;
            }
        }


    }
}
